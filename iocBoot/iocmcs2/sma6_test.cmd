#!../../bin/linux-x86_64/mcs2

## You may have to change mcs2 to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/mcs2.dbd"
mcs2_registerRecordDeviceDriver pdbbase

## test env
epicsEnvSet ("DEVIP", "192.168.1.14")
epicsEnvSet ("DEVPORT", "55551")
epicsEnvSet ("IOCBL", "LPQ")
epicsEnvSet ("IOCDEV", "sma2")

## device setup
drvAsynIPPortConfigure("SMA", "$(DEVIP):$(DEVPORT)", 0, 0, 0)

# PORT, MCS_PORT, number of axes, active poll period (ms), idle poll period (ms)
MCS2CreateController("MCS2", "SMA", 18, 250, 1000)

## Load record instances
dbLoadRecords("${EXSUB}/db/exsub.db","P=$(IOCBL):,R=$(IOCDEV):exsub")

cd "${TOP}/iocBoot/${IOC}"

dbLoadTemplate("motor.substitutions_6.test", "BL=$(IOCBL),DEV=$(IOCDEV)")

set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings_6.sav")

iocInit

create_monitor_set("auto_settings_6.req", 30, "P=$(IOCBL):$(IOCDEV)")

## Start any sequence programs
#seq sncxxx,"user=epics"
