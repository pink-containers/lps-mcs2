# LPS-mcs2

Container with Smaract MCS2 EPICS IOC

## Docker-compose.yml
```yml
version: "3.7"
services:
  ioc:
    image: docker.gitlab.gwdg.de/pink-containers/lps-mcs2/lpsmcs2:latest
    container_name: sma1
    network_mode: host
    stdin_open: true
    tty: true
    restart: always
    working_dir: "/EPICS/IOCs/lps-mcs2/iocBoot/iocmcs2"
    command: "./sma6.cmd"
    volumes:
      - type: bind
        source: /EPICS/autosave/sma1
        target: /EPICS/autosave
    environment:
      - DEVIP=0.0.0.0
      - DEVPORT=55551
      - IOCBL=LPQ
      - IOCDEV=sma1

```
